from django.core.management.base import BaseCommand, CommandError
from django.apps import apps


"""
(assuming that there is a Deployment model in the Portal App with a sync() function in its Manager)

Ways to execute

python manage.py sync deployments
python manage.py sync deployment
python manage.py sync portal.deployment

(assuming that there is a Deployment model in the Portal App with a sync_extra() function in its Manager)

python manage.py sync deployments[extra]
python manage.py sync deployments[sync_extra]
"""


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('app_model_name', nargs='+', type=str)

    def handle(self, *args, **options):
        model_index = {}
        duplicate_keys = []

        for model in apps.get_models():
            # print('adding model: ', f'{model._meta.app_label}.{model._meta.model_name}')
            names = [
                f'{model._meta.app_label}.{model._meta.model_name}',
                model._meta.model_name,
                model._meta.verbose_name,
                model._meta.verbose_name_plural,
            ]
            if len(model._meta.verbose_name.split(" ")) > 1:
                names.append('_'.join(model._meta.verbose_name.split(" ")))
            if len(model._meta.verbose_name_plural.split(" ")) > 1:
                names.append('_'.join(model._meta.verbose_name_plural.split(" ")))
                names.append(''.join(model._meta.verbose_name_plural.split(" ")))

            for full_name in names:
                if full_name in model_index.keys():
                    duplicate_keys.append(full_name)
                    model_index[full_name].append(model)
                else:
                    model_index[full_name] = [model]

        for model_name in options['app_model_name']:

            if '[' in model_name and model_name[-1] == ']':
                model_name, sync_method = model_name[:-1].split('[')
                if not sync_method.startswith('sync_'):
                    sync_method = 'sync_' + sync_method
            else:
                sync_method = 'sync'

            if model_name in model_index.keys():
                if model_name in duplicate_keys:
                    raise CommandError("Please use a reference name that isn't duplicated")
                model = model_index[model_name][0]
                manager = model.objects
                if hasattr(manager, sync_method):
                    print(f'Syncing {model._meta.verbose_name_plural}')
                    getattr(manager, sync_method)()
                else:
                    raise CommandError('unable to sync for this object.')
            else:
                raise CommandError('Model is not in listed group')

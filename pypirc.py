import os

pypi_template = """
[distutils]
index-servers = 
  pypi-test
  pypi

[pypi-test]
repository: https://test.pypi.org/legacy/
username: {username}
password: {password_test}

[pypi]
repository: https://pypi.org/legacy/
username: {username}
password: {password}
"""


def main():
    home_dir = os.environ['HOME']
    username = os.environ['PYPI_USERNAME']
    password = os.environ['PYPI_PASSWORD']
    password_test = os.environ['PYPI_PASSWORD_TEST']

    with open(os.path.join(home_dir, '.pypirc'), 'w') as f:
        f.write(pypi_template.format(username=username, password=password, password_test=password_test))


if __name__ == '__main__':
    main()

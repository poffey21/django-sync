from django.db import models

# Create your models here.

class SampleQuerySet(models.QuerySet):
    """

    """
    
    def sync(self):
        print('Running sync for sample model')
 
    def sync_unique(self):
        print('running additional sync')


class Sample(models.Model):
    """
    Provides a simple implementation of how
    sync runs
    """
    name = models.CharField(max_length=32)
    description = models.TextField(blank=True)

    objects = SampleQuerySet.as_manager()


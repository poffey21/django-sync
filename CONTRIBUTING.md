## How To Contribute



## How This Was Created

```
git clone git@gitlab.com:poffey21/django-sync.git
cd django-sync
virtualenv .venv
source .venv/bin/activate
django-admin startproject www
mv www w
mv w/* .
rmdir w
python manage.py startapp sync
python manage.py startapp testapp
# add apps to www/settings.py > INSTALLED_APPS
mkdir -p sync/management/commands/
touch sync/management/__init__.py
touch sync/management/commands/__init__.py
touch sync/management/commands/sync.py
rm sync/views.py
rm sync/admin.py
touch LICENSE
touch sync/LICENSE

```
